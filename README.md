# Docker PHP Tutorial
[![Build Status](https://travis-ci.org/kadnan/DockerPHPTutorial.svg?branch=master)](https://travis-ci.org/kadnan/DockerPHPTutorial)

# Pasos para la instalación del sistema Versión PHP 5:

Ejecutar el docker-compose up

Conectarse a un DBMS de MySQL y crear la base de datos my_db
Dentro de esta base de datos my_db, crear la tabla test con la siguiente instrucción:
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8
